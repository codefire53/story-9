$(document).ready(function (){
    var query="Sophie's World";
    search_bar(query)
});
$(document).on("click", "#search-button", function(event){
    event.preventDefault();
    search_bar(null);
});
function search_bar(query) {
    var book=query;
    if(query==null){
        book = $("#search-query").val();
    }

        $.ajax({
            url:'https://www.googleapis.com/books/v1/volumes?q='+book,
            data:{
                'query': book
            },
            dataType: 'json',
    
            success:function (result) {
                if ($.trim($("#books-list").html()).length != 0 ) {
                    $("#books-list").empty();
                }

                for (i = 0; i < result.items.length; i++) {
                    var title=result.items[i].volumeInfo.title;
                    try{
                        var img=result.items[i].volumeInfo.imageLinks.smallThumbnail;
                        var flag=true;
                    }
                    catch{
                        var flag=false;
                    }
                    var desc=result.items[i].volumeInfo.description;
                    var authors=result.items[i].volumeInfo.authors;
                    if(title==undefined){
                        title="title not found";
                    }
                    if(desc==undefined){
                        desc="description not found";
                    }
                    if(authors==undefined || authors.length == 0){
                        var author="author not found"
                    }
                    var author=authors.join(", ");
                    if(Boolean(flag)){
                        $("#books-list").append(
                            "<tr>"+"<td>"+title+"</td>"+"<td><img src="+img+"></td>"+"<td>"+desc+"</td>"+"<td>"+author+"</td>"+"</tr>"
                            );
                    }
                    else{
                        $("#books-list").append(
                            "<tr>"+"<td>"+title+"</td>"+"<td>image not found</td>"+"<td>"+desc+"</td>"+"<td>"+author+"</td>"+"</tr>"
                            );
                    } 
                }
            }
        })
        return false;
}
