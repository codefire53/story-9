from django.test import TestCase
from django.test import TestCase,LiveServerTestCase,Client
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
from django.contrib.auth.models import User
from django.urls import resolve
import chromedriver_binary
from .views import index
# Create your tests here.
class IndexUnitTest(TestCase):
    def test_index_exist(self):
        response=Client().get('')
        self.assertEqual(response.status_code,200)
    def test_index_template(self):
        response=Client().get('')
        self.assertTemplateUsed(response,'index.html')
    def test_login_exist(self):
        response=Client().get('/%2Flogin/')
        self.assertEqual(response.status_code,200)
    def test_login_template(self):
        response=Client().get('/%2Flogin/')
        self.assertTemplateUsed(response,'registration/login.html')
    def test_error_page(self):
        response=Client().get('/error/')
        self.assertEqual(response.status_code,404)

class LoginFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        #self.selenium=webdriver.Chrome()

    def tearDown(self):
        self.selenium.quit()

    def test_page_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        time.sleep(3)
        self.assertIn('login', self.selenium.page_source)
    
    def test_login_logout_works_selenium(self):
        user = User.objects.create_user('mki', 'mki590@gmail.com', '123123')
        user.set_password('123123')
        user.save()
        selenium = self.selenium
        selenium.get(self.live_server_url)

        login_button = selenium.find_element_by_id('login_button')
        login_button.click()

        time.sleep(5)

        username = selenium.find_element_by_id('id_username')
        username.send_keys("mki")

        password = selenium.find_element_by_id('id_password')
        password.send_keys("123123")

        submit_button = selenium.find_element_by_id('login_button')
        submit_button.click()

        time.sleep(5)

        self.assertIn('mki', self.selenium.page_source)

        logout_button = selenium.find_element_by_id('logout_button')
        logout_button.click()

        time.sleep(5)

        self.assertIn('Login', self.selenium.page_source)
