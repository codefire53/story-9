from django.shortcuts import render
from django.contrib import messages
# Create your views here.
def index(request):
    response = {'user':request.user}
    return render(request, 'index.html', response)
